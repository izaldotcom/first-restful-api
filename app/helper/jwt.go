package helper

import (
	"fmt"
	"os"
	"time"

	jwt "github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
)

type DataJWT struct {
	jwt.StandardClaims
	UserUUID string `json:"user_uuid"`
	Username string `json:"username"`
	Email    string `json:"email"`
	RoleName string `json:"role_name"`
	Phone    string `json:"phone"`
}

var ApplicationName = "Klipz Backend"
var LoginExpirationDuration = time.Duration(4) * time.Hour
var JwtSigningMethod = jwt.SigningMethodHS256
var JwtSignatureKey = []byte(os.Getenv("API_JWT_SECRET"))

func GenerateJWT(userUuid, username, email, roleName, phone string) (string, error) {
	claims := DataJWT{
		StandardClaims: jwt.StandardClaims{
			Issuer:    ApplicationName,
			ExpiresAt: time.Now().Add(LoginExpirationDuration).Unix(),
		},
		UserUUID: userUuid,
		Username: username,
		Email:    email,
		RoleName: roleName,
		Phone:    phone,
	}

	token := jwt.NewWithClaims(
		JwtSigningMethod,
		claims,
	)

	signedToken, err := token.SignedString(JwtSignatureKey)
	if err != nil {
		return "", err
	}

	return signedToken, nil
}

func ValidateAuthToken(encodedToken string) (*jwt.Token, error) {
	return jwt.Parse(encodedToken, func(token *jwt.Token) (interface{}, error) {
		if _, isValid := token.Method.(*jwt.SigningMethodHMAC); !isValid {
			return nil, fmt.Errorf("invalid token")
		}
		return JwtSignatureKey, nil
	})
}

func ExtractJWTAuth(c *gin.Context) (map[string]string, error) {
	getClaims, _ := c.Get("my_user_model")
	// if !ok {
	// 	return nil, errors.New("claims not found")
	// }

	convClaims, _ := getClaims.(jwt.MapClaims)
	// if !ok {
	// 	return nil, errors.New("invalid claims")
	// }

	handleMaps := map[string]string{}

	for key, val := range convClaims {
		switch key {
		case "user_uuid":
			handleMaps["user_uuid"] = val.(string)
		case "username":
			handleMaps["username"] = val.(string)
		case "email":
			handleMaps["email"] = val.(string)
		case "role_name":
			handleMaps["role_name"] = val.(string)
		}
	}

	return handleMaps, nil
}
