package helper

import (
	"reflect"

	"github.com/twinj/uuid"
)

func ValidateUuid(data string) bool {
	_, err := uuid.Parse(data)

	return err == nil
}

func ValidateIsZeroUnderlyingType(x interface{}) bool {
	return reflect.DeepEqual(x, reflect.Zero(reflect.TypeOf(x)).Interface())
}

func ValidateUserRole(currUserRole, expectRole string) bool {
	return currUserRole == expectRole
}

func ContainsString(s []string, str string) bool {
	for _, v := range s {
		if v == str {
			return true
		}
	}

	return false
}
