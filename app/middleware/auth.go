package middleware

import (

	// "gorm.io/gorm"
	"net/http"

	"github.com/jinzhu/gorm"

	helper "first-restful-api/app/helper"

	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
)

func JWTAuthMiddleware(auto401 bool, databaseConnection *gorm.DB) gin.HandlerFunc {
	return func(c *gin.Context) {
		response := helper.Response{C: c}
		c.Set("my_user_model", 0)
		authHeader := c.GetHeader("access_token")
		if auto401 {
			if len(authHeader) <= 0 {
				response.ResponseFormatter(http.StatusUnauthorized, "Invalid Access Token. Re-login now", nil, nil)
				c.Abort()
				return
			}

			token, err := helper.ValidateAuthToken(authHeader)
			if err != nil {
				response.ResponseFormatter(http.StatusUnauthorized, "Invalid Access Token. Re-login now", err, nil)
				c.Abort()
				return
			}

			claims := token.Claims.(jwt.MapClaims)
			// if val, ok := claims["phone_id"]; ok && val != "" {
			// 	getPhoneID, err := controller.NewAnonymousController(databaseConnection).GetByPhoneID(val.(string))
			// 	if errors.Is(err, gorm.ErrRecordNotFound) {
			// 		response.ResponseFormatter(http.StatusUnauthorized, "Access Token was Used On Another Device. Re-login now", err, nil)
			// 		c.Abort()
			// 		return
			// 	}
			// 	log.Println(getPhoneID.PhoneID)
			// }
			c.Set("my_user_model", claims)
		}

		c.Next()
	}
}
