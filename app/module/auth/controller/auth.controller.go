package controller

import (
	model "first-restful-api/app/module/auth/model"
	migrate "first-restful-api/database"

	"github.com/jinzhu/gorm"
)

func Save(refTkn migrate.RefreshToken) (result *gorm.DB) {
	result = model.Save(refTkn)

	return result
}

func GetOtpByCode(otp string) (migrate.OtpTrans, error) {
	data, err := model.GetOtpByCode(otp)
	if err != nil {
		return migrate.OtpTrans{}, err
	}

	return data, nil
}

func GetOtpByEmail(email string) (migrate.OtpTrans, error) {
	data, err := model.GetOtpByEmail(email)
	if err != nil {
		return migrate.OtpTrans{}, err
	}

	return data, nil
}

func GetOtpByPhone(phone string) (migrate.OtpTrans, error) {
	data, err := model.GetOtpByPhone(phone)
	if err != nil {
		return migrate.OtpTrans{}, err
	}

	return data, nil
}

func GetDataOtpByParams(otp string, email string, phone string) (migrate.OtpTransWithUser, error) {
	data, err := model.GetDataOtpByParams(otp, email, phone)
	if err != nil {
		return migrate.OtpTransWithUser{}, err
	}

	return data, nil
}

func GetOtpByParams(params string, tipe int) (migrate.OtpTrans, error) {
	data, err := model.GetOtpByParams(params, tipe)
	if err != nil {
		return migrate.OtpTrans{}, err
	}

	return data, nil
}

func GetDataUserHasRole(user_uuid string) (migrate.UserHasRoles, error) {
	data, err := model.GetDataUserHasRole(user_uuid)
	if err != nil {
		return migrate.UserHasRoles{}, err
	}

	return data, nil
}

// func Save(refTkn migrate.RefreshToken) (getToken string, result *gorm.DB, err error) {
// 	getToken, result = model.Save(refTkn)
// 	if result.Error != nil {
// 		return "", nil, result.Error
// 	}
// 	return getToken, result, nil
// }
