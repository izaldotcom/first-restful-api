package handler

import (
	"errors"
	"net/http"

	helper "first-restful-api/app/helper"
	authController "first-restful-api/app/module/auth/controller"
	validator "first-restful-api/app/module/auth/validator"
	otpController "first-restful-api/app/module/otp/controller"
	otpModel "first-restful-api/app/module/otp/model"
	roleController "first-restful-api/app/module/role/controller"
	userController "first-restful-api/app/module/user/controller"
	migrate "first-restful-api/database"

	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
)

type AuthHandler struct {
	DB *gorm.DB
}

func NewAuthHandler(DB *gorm.DB) AuthHandler {
	return AuthHandler{DB: DB}
}

func Login(ctx *gin.Context) {
	var (
		paramsValidator string
		responValidator string
	)

	loginValidator := validator.NewLoginValidator()

	if err := loginValidator.Bind(ctx); err != nil {
		helper.NewResponse(ctx).ResponseFormatter(http.StatusBadRequest, "Invalid Form", err, gin.H{"error": map[string]string{"binding_error": err.Error()}})

		return
	}

	if loginValidator.Email != "" {
		paramsValidator = loginValidator.Email
		responValidator = "Email"
	}

	if loginValidator.Phone != "" {
		paramsValidator = loginValidator.Phone
		responValidator = "Phone"
	}

	getUser, err := userController.GetUserByParams(paramsValidator)
	getDataUserHasRole, _ := authController.GetDataUserHasRole(getUser.UUID)

	if getUser.Email == "" || getUser.Phone == "" {
		helper.NewResponse(ctx).ResponseFormatter(http.StatusBadRequest, "User is not existed ", err, gin.H{"error": map[string]string{"get_user_error": "User is not existed"}})
		return
	}

	if errors.Is(err, gorm.ErrRecordNotFound) {
		helper.NewResponse(ctx).ResponseFormatter(http.StatusBadRequest, "Incorrect "+responValidator, err, gin.H{"error": map[string]string{"get_verified_user_error": "Incorrect " + responValidator}})
		return
	}

	if !helper.CheckPasswordHash(loginValidator.Password, getUser.Password) {
		helper.NewResponse(ctx).ResponseFormatter(http.StatusBadRequest, "Incorrect Password", nil, gin.H{"error": map[string]string{"password_error": "Incorrect Password"}})
		return
	}

	token, err := helper.GenerateJWT(getUser.UUID, getUser.Username, getUser.Email, getDataUserHasRole.RoleUUID, getUser.Phone)
	if err != nil {
		helper.NewResponse(ctx).ResponseFormatter(http.StatusInternalServerError, "Failed Create Token JWT", err, gin.H{
			"error": map[string]string{"create_jwt_error": err.Error()},
		})
		return
	}

	getRole, _ := roleController.GetRoleByUuid(getDataUserHasRole.RoleUUID)

	helper.NewResponse(ctx).ResponseFormatter(http.StatusOK, "Login Successfully", nil, gin.H{
		"access_token": token,
		"user": map[string]interface{}{
			"uuid":      getUser.UUID,
			"username":  getUser.Username,
			"email":     getUser.Email,
			"role_name": getRole.Name,
		},
	})

	return
}

func Register(ctx *gin.Context) {

	registerValidator := validator.NewRegisterValidator()

	if err := registerValidator.Bind(ctx); err != nil {
		helper.NewResponse(ctx).ResponseFormatter(http.StatusBadRequest, "Invalid Form", err, gin.H{"error": map[string]string{"binding_error": err.Error()}})

		return
	}

	if registerValidator.Username == "" || registerValidator.Email == "" {
		helper.NewResponse(ctx).ResponseFormatter(http.StatusBadRequest, "Invalid Form", nil, gin.H{"error": map[string]string{"binding_error": "Data Required"}})
		return
	}

	if registerValidator.Password != registerValidator.ConfirmPassword {
		helper.NewResponse(ctx).ResponseFormatter(http.StatusBadRequest, "Confirm Password Not Match", nil, gin.H{"error": map[string]string{
			"confirm_password": "not match",
		}})

		return
	}

	hashPassword, _ := helper.HashPassword(registerValidator.Password)
	_, checkUsername := userController.GetUserByUsername(registerValidator.Username)

	if errors.Is(checkUsername, gorm.ErrRecordNotFound) {

		_, checkEmail := userController.GetUserByEmail(registerValidator.Email)

		if errors.Is(checkEmail, gorm.ErrRecordNotFound) {

			// save register user
			newTmpUser := migrate.NewTmpUser()

			newTmpUser.Username = registerValidator.Username
			newTmpUser.Email = registerValidator.Email
			newTmpUser.Password = hashPassword

			saveRegisterTmpUser := userController.SaveTmp(newTmpUser)
			// end save register user

			if saveRegisterTmpUser != nil {
				helper.NewResponse(ctx).ResponseFormatter(http.StatusOK, "Register Successfully", nil, gin.H{
					"data": registerValidator,
				})

				return
			}

		}

		helper.NewResponse(ctx).ResponseFormatter(http.StatusBadRequest, "Email Already Existed", nil, gin.H{"error": map[string]string{
			"email_error": "Email Already Existed",
		}})

	}

	if checkUsername == nil {
		helper.NewResponse(ctx).ResponseFormatter(http.StatusBadRequest, "Username Already Existed", nil, gin.H{"error": map[string]string{
			"username_error": "Username Already Existed",
		}})
		return
	}
}

func SendOtpRegister(ctx *gin.Context) {

	sendotpValidator := validator.NewSendOtpRegisterValidator()

	if err := sendotpValidator.Bind(ctx); err != nil {
		helper.NewResponse(ctx).ResponseFormatter(http.StatusBadRequest, "Invalid Form", err, gin.H{"error": map[string]string{"binding_error": err.Error()}})

		return
	}

	_, err := userController.GetTmpUserByEmail(sendotpValidator.Email)
	if err != nil {
		helper.NewResponse(ctx).ResponseFormatter(http.StatusBadRequest, "Incorrect email", err, gin.H{"error": map[string]string{"get_respone_error": "Incorrect email"}})
		return
	}

	_, checkOtp := authController.GetOtpByEmail(sendotpValidator.Email)

	if errors.Is(checkOtp, gorm.ErrRecordNotFound) {
		// save otp register user
		// getDataUser, _ := userController.GetUserByEmail(registerValidator.Email)
		otpCode := helper.GenerateRandomNumber(4)
		newOtpSendByPhone := migrate.NewOtpTrans()

		newOtpSendByPhone.Email = sendotpValidator.Email
		newOtpSendByPhone.Type = otpModel.RegisterUser
		newOtpSendByPhone.Code = otpCode
		newOtpSendByPhone.Phone = sendotpValidator.Phone
		newOtpSendByPhone.Description = "OTP for register user"

		saveOtpRegisterUser := otpController.Save(newOtpSendByPhone)
		// end save otp register user

		if saveOtpRegisterUser != nil {
			helper.NewResponse(ctx).ResponseFormatter(http.StatusOK, "OTP Successfully Created", nil, gin.H{
				"data":     sendotpValidator,
				"otp_code": otpCode,
			})

			return
		}
	}

	helper.NewResponse(ctx).ResponseFormatter(http.StatusBadRequest, "Email Already Existed", nil, gin.H{"error": map[string]string{
		"email_error": "Email Already Existed",
	}})
}

func VerifyRegister(ctx *gin.Context) {

	verifyRegisterValidator := validator.NewVerifyOtpValidator()

	if err := verifyRegisterValidator.Bind(ctx); err != nil {
		helper.NewResponse(ctx).ResponseFormatter(http.StatusBadRequest, "Invalid Form", err, gin.H{"error": map[string]string{"binding_error": err.Error()}})

		return
	}

	getDataOtp, err := authController.GetOtpByCode(verifyRegisterValidator.OtpCode)

	if getDataOtp.Code == "" {
		helper.NewResponse(ctx).ResponseFormatter(http.StatusBadRequest, "OTP code is not existed ", err, gin.H{"error": map[string]string{"get_otp_error": "OTP code is not existed"}})
		return
	}

	checkUser, _ := userController.GetTmpUserVerifByEmail(getDataOtp.Email)
	if checkUser != "" {
		helper.NewResponse(ctx).ResponseFormatter(http.StatusInternalServerError, "Failed Process Verification", nil, gin.H{"error": map[string]string{"verification_error": "Failed Process Verification"}})
		return
	}

	checkData, _ := userController.VerifyRegister(getDataOtp.Email)
	helper.NewResponse(ctx).ResponseFormatter(http.StatusOK, "Verify Register User Successfully", nil, gin.H{
		"data": "Verify Register User Successfully",
	})

	if checkData != "" {
		getDataTmpUserByEmail, _ := userController.GetDataTmpUserVerifByEmail(checkData)
		// move tmp user to user
		newUser := migrate.NewUser()

		newUser.Username = getDataTmpUserByEmail.Username
		newUser.Email = getDataOtp.Email
		newUser.Password = getDataTmpUserByEmail.Password
		newUser.Phone = getDataOtp.Phone

		userController.Save(newUser)
		// end save tmp user to user

		getUser, _ := userController.GetUserByParams(getDataOtp.Email)
		getRole := roleController.GetRoleByName("customer")
		// save user has role
		newUserHasRole := migrate.NewUserHasRoles()

		newUserHasRole.UserUUID = getUser.UUID
		newUserHasRole.RoleUUID = getRole.UUID

		roleController.SaveUserHasRole(newUserHasRole)
		// end save
	}
}

func SendOtpResetPassword(ctx *gin.Context) {

	var (
		paramsValidator string
		responValidator string
	)

	sendotpValidator := validator.NewSendOtpResetPasswordValidator()

	if err := sendotpValidator.Bind(ctx); err != nil {
		helper.NewResponse(ctx).ResponseFormatter(http.StatusBadRequest, "Invalid Form", err, gin.H{"error": map[string]string{"binding_error": err.Error()}})

		return
	}

	if sendotpValidator.Email != "" {
		paramsValidator = sendotpValidator.Email
		responValidator = "Email"
	}

	if sendotpValidator.Phone != "" {
		paramsValidator = sendotpValidator.Phone
		responValidator = "Phone"
	}

	getUser, err := userController.GetUserByParams(paramsValidator)

	if getUser.Email == "" || getUser.Phone == "" {
		helper.NewResponse(ctx).ResponseFormatter(http.StatusBadRequest, "User is not existed ", err, gin.H{"error": map[string]string{"get_user_error": "User is not existed"}})
		return
	}

	if errors.Is(err, gorm.ErrRecordNotFound) {
		helper.NewResponse(ctx).ResponseFormatter(http.StatusBadRequest, "Incorrect "+responValidator, err, gin.H{"error": map[string]string{"get_verified_user_error": "Incorrect " + responValidator}})
		return
	}

	// save otp reset password
	otpCode := helper.GenerateRandomNumber(4)
	newOtpSendByPhone := migrate.NewOtpTrans()

	newOtpSendByPhone.Email = getUser.Email
	newOtpSendByPhone.Type = otpModel.ForgotPassword
	newOtpSendByPhone.Code = otpCode
	newOtpSendByPhone.Phone = getUser.Phone
	newOtpSendByPhone.Description = "OTP for forgot password"

	saveOtpRegisterUser := otpController.Save(newOtpSendByPhone)
	// endsave

	// save tmp password
	getDataOtp, _ := authController.GetOtpByCode(otpCode)
	newTmpPassword := migrate.NewTmpPassword()

	newTmpPassword.Email = getDataOtp.Email
	saveTmpPassword := userController.SaveTmpPassword(newTmpPassword)
	// end save tmp password

	if saveOtpRegisterUser != nil && saveTmpPassword != nil {
		helper.NewResponse(ctx).ResponseFormatter(http.StatusOK, "OTP Successfully Created", nil, gin.H{
			"otp_code": otpCode,
		})

		return
	}
}

func VerifyResetPassword(ctx *gin.Context) {

	verifyResetPasswordValidator := validator.NewVerifyOtpValidator()

	if err := verifyResetPasswordValidator.Bind(ctx); err != nil {
		helper.NewResponse(ctx).ResponseFormatter(http.StatusBadRequest, "Invalid Form", err, gin.H{"error": map[string]string{"binding_error": err.Error()}})

		return
	}

	getDataOtp, err := authController.GetOtpByCode(verifyResetPasswordValidator.OtpCode)
	if getDataOtp.Code == "" {
		helper.NewResponse(ctx).ResponseFormatter(http.StatusBadRequest, "OTP code is not existed ", err, gin.H{"error": map[string]string{"get_otp_error": "OTP code is not existed"}})
		return
	}

	userController.VerifyResetPassword(getDataOtp.Email)
	helper.NewResponse(ctx).ResponseFormatter(http.StatusOK, "Verify Reset Password Successfully", nil, gin.H{
		"data": "Verify Reset Password Successfully",
	})
}

func ResetPassword(ctx *gin.Context) {

	resetPasswordValidator := validator.NewResetPasswordValidator()

	if err := resetPasswordValidator.Bind(ctx); err != nil {
		helper.NewResponse(ctx).ResponseFormatter(http.StatusBadRequest, "Invalid Form", err, gin.H{"error": map[string]string{"binding_error": err.Error()}})

		return
	}

	if resetPasswordValidator.Password != resetPasswordValidator.ConfirmPassword {
		helper.NewResponse(ctx).ResponseFormatter(http.StatusBadRequest, "Confirm Password Not Match", nil, gin.H{"error": map[string]string{
			"confirm_password": "not match",
		}})

		return
	}

	getDataTmpPassword, err := userController.GetDataTmpPasswordVerifByEmail(resetPasswordValidator.Email)
	if getDataTmpPassword.Email == "" {
		helper.NewResponse(ctx).ResponseFormatter(http.StatusBadRequest, "Data is not existed ", err, gin.H{"error": map[string]string{"get_response_error": "Data is not existed"}})
		return
	}

	getDataTmpPasswordChange, err := userController.GetDataTmpPasswordChangeByEmail(resetPasswordValidator.Email)
	if getDataTmpPasswordChange.Email == "" {
		helper.NewResponse(ctx).ResponseFormatter(http.StatusBadRequest, "Unable to reset password, please follow the step correctly", err, gin.H{"error": map[string]string{"get_response_error": "Unable to reset password, please follow the step correctly"}})
		return
	}

	userController.IsChangePassword(getDataTmpPassword.Email)
	userController.ResetPassword(getDataTmpPassword.Email, resetPasswordValidator.Password)
	helper.NewResponse(ctx).ResponseFormatter(http.StatusOK, "Reset Password Successfully", nil, gin.H{
		"data": resetPasswordValidator,
	})
}
