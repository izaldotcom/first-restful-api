package model

import (
	config "first-restful-api/app/config"
	migrate "first-restful-api/database"

	_ "github.com/go-sql-driver/mysql"
	"github.com/jinzhu/gorm"
)

func Save(refTkn migrate.RefreshToken) (result *gorm.DB) {
	result = config.DB.Create(&refTkn)

	return
}

func GetOtpByCode(otp string) (migrate.OtpTrans, error) {
	getData := migrate.NewOtpTrans()

	result := config.DB.Where("code = ?", otp).First(&getData)
	if result.Error != nil {
		return migrate.OtpTrans{}, result.Error
	}
	return getData, nil
}

func GetOtpByEmail(email string) (migrate.OtpTrans, error) {
	getData := migrate.NewOtpTrans()

	result := config.DB.Where("email = ?", email).First(&getData)
	if result.Error != nil {
		return migrate.OtpTrans{}, result.Error
	}
	return getData, nil
}

func GetOtpByPhone(phone string) (migrate.OtpTrans, error) {
	getData := migrate.NewOtpTrans()

	result := config.DB.Where("phone = ?", phone).First(&getData)
	if result.Error != nil {
		return migrate.OtpTrans{}, result.Error
	}
	return getData, nil
}

func GetOtpByParams(params string, tipe int) (migrate.OtpTrans, error) {

	getData := migrate.NewOtpTrans()

	result := config.DB.Where("type = ?", tipe).Where("email = ? OR phone = ?", params, params).Order("created_at desc").Limit(1).First(&getData)
	if result.Error != nil {
		return migrate.OtpTrans{}, result.Error
	}
	return getData, nil
}

func GetDataUserHasRole(user_uuid string) (migrate.UserHasRoles, error) {
	getData := migrate.NewUserHasRoles()

	result := config.DB.Where("user_uuid = ?", user_uuid).First(&getData)
	if result.Error != nil {
		return migrate.UserHasRoles{}, result.Error
	}
	return getData, nil
}

func GetDataOtpByParams(otp string, email string, phone string) (migrate.OtpTransWithUser, error) {
	var (
		getData migrate.OtpTransWithUser
		result  *gorm.DB
	)

	result = config.DB.Raw(`SELECT otp.uuid, otp.email, otp.type, otp.code, otp.phone, otp.description, tmp_user.name as fullname, tmp_user.username, tmp_user.email
									FROM otp_trans otp
									LEFT JOIN tmp_user on otp.email = tmp_user.email
									WHERE otp.code=? AND tmp_user.email=? AND otp.phone=? AND tmp_user.is_verification=0`, otp, email, phone).First(&getData)
	if result.Error != nil {
		return migrate.OtpTransWithUser{}, result.Error
	}
	return getData, nil
}
