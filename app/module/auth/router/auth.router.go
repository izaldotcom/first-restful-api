package router

import (
	authHandler "first-restful-api/app/module/auth/handler"

	"github.com/gin-gonic/gin"
)

func Routes(route *gin.Engine) {
	auth := route.Group("/api/v1/auth")
	{
		auth.POST("login", authHandler.Login)
		auth.POST("register", authHandler.Register)
		auth.POST("otp-register", authHandler.SendOtpRegister)
		auth.POST("verify-register", authHandler.VerifyRegister)
		auth.POST("otp-reset-password", authHandler.SendOtpResetPassword)
		auth.POST("verify-reset-password", authHandler.VerifyResetPassword)
		auth.POST("reset-password", authHandler.ResetPassword)
	}
}
