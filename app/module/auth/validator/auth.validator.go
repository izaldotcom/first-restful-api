package validator

import "github.com/gin-gonic/gin"

type LoginValidator struct {
	Email    string `form:"email" json:"email"`
	Phone    string `form:"phone" json:"phone"`
	Password string `form:"password" json:"password" binding:"required"`
}

type RegisterValidator struct {
	Username        string `form:"username" json:"username" binding:"required"`
	Email           string `form:"email" json:"email" binding:"required"`
	Password        string `json:"password" form:"password" binding:"required,min=8"`
	ConfirmPassword string `json:"confirm_password" form:"confirm_password" binding:"required,min=8"`
}

type SendOtpRegisterValidator struct {
	Email string `form:"email" json:"email" binding:"required"`
	Phone string `form:"phone" json:"phone" binding:"required"`
}

type SendOtpResetPasswordValidator struct {
	Email string `form:"email" json:"email"`
	Phone string `form:"phone" json:"phone"`
}

type VerificationValidator struct {
	OtpCode string `form:"otp_code" json:"otp_code" binding:"required"`
	Email   string `form:"email" json:"email" binding:"required"`
	Phone   string `form:"phone" json:"phone" binding:"required"`
}

type VerifyOtpValidator struct {
	OtpCode string `form:"otp_code" json:"otp_code" binding:"required"`
}

type ResetPasswordValidator struct {
	Email           string `form:"email" json:"email" binding:"required"`
	Password        string `json:"password" form:"password" binding:"required,min=8"`
	ConfirmPassword string `json:"confirm_password" form:"confirm_password" binding:"required,min=8"`
}

func NewLoginValidator() LoginValidator {
	return LoginValidator{}
}

func NewRegisterValidator() RegisterValidator {
	return RegisterValidator{}
}

func NewVerificationValidator() VerificationValidator {
	return VerificationValidator{}
}

func NewVerifyOtpValidator() VerifyOtpValidator {
	return VerifyOtpValidator{}
}

func NewSendOtpRegisterValidator() SendOtpRegisterValidator {
	return SendOtpRegisterValidator{}
}

func NewSendOtpResetPasswordValidator() SendOtpResetPasswordValidator {
	return SendOtpResetPasswordValidator{}
}

func NewResetPasswordValidator() ResetPasswordValidator {
	return ResetPasswordValidator{}
}

func (loginValidator *LoginValidator) Bind(c *gin.Context) (err error) {
	err = c.ShouldBindJSON(&loginValidator)
	if err != nil {
		return err
	}

	return
}

func (registerValidator *RegisterValidator) Bind(c *gin.Context) (err error) {
	err = c.ShouldBindJSON(&registerValidator)
	if err != nil {
		return err
	}

	return
}

func (sendOtpRegisterValidator *SendOtpRegisterValidator) Bind(c *gin.Context) (err error) {
	err = c.ShouldBindJSON(&sendOtpRegisterValidator)
	if err != nil {
		return err
	}

	return
}

func (sendOtpResePasswordValidator *SendOtpResetPasswordValidator) Bind(c *gin.Context) (err error) {
	err = c.ShouldBindJSON(&sendOtpResePasswordValidator)
	if err != nil {
		return err
	}

	return
}

func (verificationValidator *VerificationValidator) Bind(c *gin.Context) (err error) {
	err = c.ShouldBindJSON(&verificationValidator)
	if err != nil {
		return err
	}

	return
}

func (verifyOtpValidator *VerifyOtpValidator) Bind(c *gin.Context) (err error) {
	err = c.ShouldBindJSON(&verifyOtpValidator)
	if err != nil {
		return err
	}

	return
}

func (resetPasswordValidator *ResetPasswordValidator) Bind(c *gin.Context) (err error) {
	err = c.ShouldBindJSON(&resetPasswordValidator)
	if err != nil {
		return err
	}

	return
}
