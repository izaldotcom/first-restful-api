package controller

import (
	model "first-restful-api/app/module/otp/model"
	migrate "first-restful-api/database"

	"github.com/jinzhu/gorm"
)

func Save(otp migrate.OtpTrans) (result *gorm.DB) {
	result = model.Save(otp)

	return result
}

// func Save(refTkn migrate.RefreshToken) (getToken string, result *gorm.DB, err error) {
// 	getToken, result = model.Save(refTkn)
// 	if result.Error != nil {
// 		return "", nil, result.Error
// 	}
// 	return getToken, result, nil
// }
