package model

import (
	config "first-restful-api/app/config"
	migrate "first-restful-api/database"

	_ "github.com/go-sql-driver/mysql"
	"github.com/jinzhu/gorm"
)

const (
	RegisterUser   = 1
	ForgotPassword = 2
)

func Save(otp migrate.OtpTrans) (result *gorm.DB) {
	result = config.DB.Create(&otp)

	return
}
