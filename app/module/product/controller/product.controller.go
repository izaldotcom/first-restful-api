package controller

import (
	helper "first-restful-api/app/helper"
	model "first-restful-api/app/module/product/model"
	migrate "first-restful-api/database"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
)

//GetProducts ... Get all products
func GetProducts(c *gin.Context) {
	var product []migrate.Product
	err := model.GetAllProducts(&product)
	if err != nil {
		helper.NewResponse(c).ResponseFormatter(http.StatusNotFound, "Failed Get List Product", nil, gin.H{})
	} else {
		helper.NewResponse(c).ResponseFormatter(http.StatusOK, "Success Get List All Product", nil, gin.H{
			"data": product,
		})
	}
}

func Save(product migrate.Product) (result *gorm.DB) {
	result = model.Save(product)

	return result
}

//GetProductByID ... Get the product by id
func GetProductByID(c *gin.Context) {
	id := c.Params.ByName("id")
	var product migrate.Product
	err := model.GetProductByID(&product, id)
	if err != nil {
		helper.NewResponse(c).ResponseFormatter(http.StatusNotFound, "Product Not Found", nil, gin.H{})
	} else {
		helper.NewResponse(c).ResponseFormatter(http.StatusOK, "Successfully Get Product", nil, gin.H{
			"data": product,
		})
	}
}

//GetProductByName ... Get the product by name
func GetProductByName(name string) (migrate.Product, error) {
	getProduct, err := model.GetProductByName(name)
	if err != nil {
		return migrate.Product{}, err
	}

	return getProduct, nil
}

//UpdateProduct ... Update the product information
func UpdateProduct(c *gin.Context) {
	var product migrate.Product
	id := c.Params.ByName("id")
	err := model.GetProductByID(&product, id)
	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{"data": product})
	}
	c.BindJSON(&product)
	err = model.UpdateProduct(&product, id)
	if err != nil {
		helper.NewResponse(c).ResponseFormatter(http.StatusNotFound, "Failed Update Product", nil, gin.H{})
	} else {
		helper.NewResponse(c).ResponseFormatter(http.StatusOK, "Successfully Update Product ", nil, gin.H{
			"data": product,
		})
	}
}

//DeleteProduct ... Delete the product
func DeleteProduct(c *gin.Context) {
	var product migrate.Product
	id := c.Params.ByName("id")
	err := model.DeleteProduct(&product, id)
	if err != nil {
		helper.NewResponse(c).ResponseFormatter(http.StatusNotFound, "Failed Delete Product", nil, gin.H{})
	} else {
		helper.NewResponse(c).ResponseFormatter(http.StatusOK, "Successfully Deleted Product ", nil, gin.H{
			"message": "Successfully Deleted Product",
		})
	}
}
