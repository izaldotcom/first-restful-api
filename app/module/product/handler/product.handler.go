package handler

import (
	"errors"
	"net/http"

	helper "first-restful-api/app/helper"
	productController "first-restful-api/app/module/product/controller"
	validator "first-restful-api/app/module/product/validator"
	migrate "first-restful-api/database"

	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
)

type ProductHandler struct {
	DB *gorm.DB
}

func NewProductHandler(DB *gorm.DB) ProductHandler {
	return ProductHandler{DB: DB}
}

// Register exis
func Create(ctx *gin.Context) {

	productValidator := validator.NewCreateProductValidator()

	if err := productValidator.Bind(ctx); err != nil {
		helper.NewResponse(ctx).ResponseFormatter(http.StatusBadRequest, "Invalid Form", err, gin.H{"error": map[string]string{"binding_error": err.Error()}})

		return
	}

	_, err := productController.GetProductByName(productValidator.Name)

	if errors.Is(err, gorm.ErrRecordNotFound) {
		newProduct := migrate.NewProduct()

		newProduct.Name = productValidator.Name
		newProduct.Price = productValidator.Price
		newProduct.Qty = productValidator.Qty

		if err := productController.Save(newProduct); err != nil {
			helper.NewResponse(ctx).ResponseFormatter(http.StatusOK, "Successfully Add Product", nil, gin.H{
				"data": productValidator,
			})

			return
		}
	}

	helper.NewResponse(ctx).ResponseFormatter(http.StatusBadRequest, "Name Already Existed", nil, gin.H{"error": map[string]string{
		"name_error": "Name Already Existed",
	}})

	return

}
