package model

import (
	config "first-restful-api/app/config"
	migrate "first-restful-api/database"
	"fmt"

	_ "github.com/go-sql-driver/mysql"
	"github.com/jinzhu/gorm"
)

//GetAllProducts Fetch all product data
func GetAllProducts(product *[]migrate.Product) (err error) {
	if err = config.DB.Find(product).Error; err != nil {
		return err
	}
	return nil
}

//CreateProduct ... Insert New data
func CreateProduct(product *migrate.Product) (err error) {
	if err = config.DB.Create(product).Error; err != nil {
		return err
	}
	return nil
}

func Save(product migrate.Product) (result *gorm.DB) {
	result = config.DB.Create(&product)

	return
}

//GetProductByID ... Fetch only one product by Id
func GetProductByID(product *migrate.Product, id string) (err error) {
	if err = config.DB.Where("id = ?", id).First(product).Error; err != nil {
		return err
	}
	return nil
}

//UpdateProduct ... Update product
func UpdateProduct(product *migrate.Product, id string) (err error) {
	fmt.Println(product)
	config.DB.Save(product)
	return nil
}

//DeleteProduct ... Delete product
func DeleteProduct(product *migrate.Product, id string) (err error) {
	config.DB.Where("id = ?", id).Delete(product)
	return nil
}

// Get Data Product By Name
func GetProductByName(name string) (migrate.Product, error) {
	getProduct := migrate.NewProduct()

	result := config.DB.Where("name = ?", name).First(&getProduct)
	if result.Error != nil {
		return migrate.Product{}, result.Error
	}
	return getProduct, nil
}
