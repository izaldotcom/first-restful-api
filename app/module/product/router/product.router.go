package router

import (
	productController "first-restful-api/app/module/product/controller"
	productHandler "first-restful-api/app/module/product/handler"

	"github.com/gin-gonic/gin"
)

func Routes(route *gin.Engine) {
	product := route.Group("/api/v1")
	{
		product.GET("product", productController.GetProducts)
		product.POST("product", productHandler.Create)
		product.GET("product/:id", productController.GetProductByID)
		product.PUT("product/:id", productController.UpdateProduct)
		product.DELETE("product/:id", productController.DeleteProduct)
	}
}
