package validator

import "github.com/gin-gonic/gin"

type CreateProductValidator struct {
	Name  string  `json:"name" binding:"required"`
	Price float64 `gorm:"column:price" json:"price" binding:"required"`
	Qty   int64   `gorm:"column:qty" json:"qty" binding:"required"`
}

func NewCreateProductValidator() CreateProductValidator {
	return CreateProductValidator{}
}

func (productValidator *CreateProductValidator) Bind(c *gin.Context) (err error) {
	err = c.ShouldBindJSON(&productValidator)
	if err != nil {
		return err
	}

	return
}
