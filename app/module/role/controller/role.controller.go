package controller

import (
	model "first-restful-api/app/module/role/model"
	migrate "first-restful-api/database"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
)

//GetUsers ... Get all users
func GetUsers(c *gin.Context) {
	var user []migrate.Role
	err := model.GetAllUsers(&user)
	if err != nil {
		c.AbortWithStatus(http.StatusNotFound)
	} else {
		c.JSON(http.StatusOK, gin.H{"data": user})
	}
}

func Save(user migrate.Role) (result *gorm.DB) {
	result = model.Save(user)

	return result
}

func SaveUserHasRole(data migrate.UserHasRoles) (result *gorm.DB) {
	result = model.SaveUserHasRole(data)

	return result
}

func GetRoleByUuid(uuid string) (migrate.Role, error) {
	getRole, err := model.GetRoleByUuid(uuid)
	if err != nil {
		return migrate.Role{}, err
	}

	return getRole, nil
}

//GetUserByName ... Get the user by name
func GetRoleByName(name string) migrate.Role {
	getRole, err := model.GetRoleByName(name)
	if err != nil {
		return migrate.Role{}
	}

	return getRole
}
