package model

import (
	config "first-restful-api/app/config"
	migrate "first-restful-api/database"

	_ "github.com/go-sql-driver/mysql"
	"github.com/jinzhu/gorm"
)

//GetAllUsers Fetch all user data
func GetAllUsers(user *[]migrate.Role) (err error) {
	if err = config.DB.Find(user).Error; err != nil {
		return err
	}
	return nil
}

func Save(user migrate.Role) (result *gorm.DB) {
	result = config.DB.Create(&user)

	return
}

func SaveUserHasRole(data migrate.UserHasRoles) (result *gorm.DB) {
	result = config.DB.Create(&data)

	return
}

func GetRoleByUuid(uuid string) (migrate.Role, error) {
	getRole := migrate.NewRole()

	result := config.DB.Where("uuid = ?", uuid).First(&getRole)
	if result.Error != nil {
		return migrate.Role{}, result.Error
	}
	return getRole, nil
}

// Get Data User By Name
func GetRoleByName(name string) (migrate.Role, error) {
	getRole := migrate.NewRole()

	result := config.DB.Where("name = ?", name).First(&getRole)
	if result.Error != nil {
		return migrate.Role{}, result.Error
	}
	return getRole, nil
}
