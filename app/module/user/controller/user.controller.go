package controller

import (
	helper "first-restful-api/app/helper"
	model "first-restful-api/app/module/user/model"
	migrate "first-restful-api/database"
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
)

//GetUsers ... Get all users
func GetUsers(c *gin.Context) {
	var user []migrate.User
	err := model.GetAllUsers(&user)
	if err != nil {
		c.AbortWithStatus(http.StatusNotFound)
	} else {
		c.JSON(http.StatusOK, gin.H{"data": user})
	}
}

func Save(user migrate.User) (result *gorm.DB) {
	result = model.Save(user)

	return result
}

func SaveTmp(user migrate.TmpUser) (result *gorm.DB) {
	result = model.SaveTmp(user)

	return result
}

func SaveTmpPassword(data migrate.TmpPassword) (result *gorm.DB) {
	result = model.SaveTmpPassword(data)

	return result
}

//CreateUser ... Create User
func CreateUser(c *gin.Context) {
	var user migrate.User

	if err := c.BindJSON(&user); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	} else {
		data := model.CreateUser(&user)
		if data != nil {
			fmt.Println(data.Error())
			c.AbortWithStatus(http.StatusNotFound)
		} else {
			helper.NewResponse(c).ResponseFormatter(http.StatusOK, "Successfully Add User", nil, gin.H{
				"data": user,
			})
		}
	}
}

//GetUserByID ... Get the user by id
func GetUserByID(c *gin.Context) {
	id := c.Params.ByName("id")
	var user migrate.User
	err := model.GetUserByID(&user, id)
	if err != nil {
		c.AbortWithStatus(http.StatusNotFound)
	} else {
		c.JSON(http.StatusOK, gin.H{"data": user})
	}
}

//GetUserByName ... Get the user by name
func GetUserByName(name string) (migrate.User, error) {
	getUser, err := model.GetUserByName(name)
	if err != nil {
		return migrate.User{}, err
	}

	return getUser, nil
}

//GetUserByEmail ... Get the user by email
func GetUserByEmail(email string) (migrate.User, error) {
	getUser, err := model.GetUserByEmail(email)
	if err != nil {
		return migrate.User{}, err
	}

	return getUser, nil
}

// Get the user tmp by email
func GetTmpUserByEmail(email string) (migrate.TmpUser, error) {
	getUser, err := model.GetTmpUserByEmail(email)
	if err != nil {
		return migrate.TmpUser{}, err
	}

	return getUser, nil
}

func GetUserByPhone(phone string) (migrate.User, error) {
	getUser, err := model.GetUserByPhone(phone)
	if err != nil {
		return migrate.User{}, err
	}

	return getUser, nil
}

func GetTmpUserByPhone(phone string) (migrate.TmpUser, error) {
	getUser, err := model.GetTmpUserByPhone(phone)
	if err != nil {
		return migrate.TmpUser{}, err
	}

	return getUser, nil
}

//GetUserByPhone ... Get the user by phone number
func GetUserByParams(params string) (migrate.User, error) {
	getUser, err := model.GetUserByParams(params)
	if err != nil {
		return migrate.User{}, err
	}

	return getUser, nil
}

func GetUserByOtp(userUuid string, otp string) (migrate.User, error) {
	getUser, err := model.GetUserByOtp(userUuid, otp)
	if err != nil {
		return migrate.User{}, err
	}

	return getUser, nil
}

func GetUserByUsername(username string) (migrate.User, error) {
	getUser, err := model.GetUserByUsername(username)
	if err != nil {
		return migrate.User{}, err
	}

	return getUser, nil
}

func GetUserByFlagSuperadmin(params int) (migrate.User, error) {
	getUser, err := model.GetUserByFlagSuperadmin(params)
	if err != nil {
		return migrate.User{}, err
	}

	return getUser, nil
}

func GetDataTmpUserVerifByEmail(email string) (migrate.TmpUser, error) {
	getUser, err := model.GetDataTmpUserVerifByEmail(email)
	if err != nil {
		return migrate.TmpUser{}, err
	}

	return getUser, nil
}

func GetDataTmpPasswordVerifByEmail(email string) (migrate.TmpPassword, error) {
	getUser, err := model.GetDataTmpPasswordVerifByEmail(email)
	if err != nil {
		return migrate.TmpPassword{}, err
	}

	return getUser, nil
}

func GetDataTmpPasswordChangeByEmail(email string) (migrate.TmpPassword, error) {
	getUser, err := model.GetDataTmpPasswordChangeByEmail(email)
	if err != nil {
		return migrate.TmpPassword{}, err
	}

	return getUser, nil
}

func GetTmpUserVerifByEmail(email string) (string, error) {
	_, err := model.GetTmpUserVerifByEmail(email)
	if err != nil {
		return "", err
	}

	return email, nil
}

//UpdateUser ... Update the user information
func UpdateUser(c *gin.Context) {
	var user migrate.User
	id := c.Params.ByName("id")
	err := model.GetUserByID(&user, id)
	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{"data": user})
	}
	c.BindJSON(&user)
	err = model.UpdateUser(&user, id)
	if err != nil {
		c.AbortWithStatus(http.StatusNotFound)
	} else {
		c.JSON(http.StatusOK, gin.H{"data": user})
	}
}

//DeleteUser ... Delete the user
func DeleteUser(c *gin.Context) {
	var user migrate.User
	id := c.Params.ByName("id")
	err := model.DeleteUser(&user, id)
	if err != nil {
		c.AbortWithStatus(http.StatusNotFound)
	} else {
		c.JSON(http.StatusOK, gin.H{"id" + id: "is deleted"})
	}
}

func VerifyRegister(email string) (string, error) {
	result, err := model.VerifyRegister(email)
	if err != nil {
		return "", err
	}

	return result, nil
}

func VerifyResetPassword(email string) (string, error) {
	result, err := model.VerifyResetPassword(email)
	if err != nil {
		return "", err
	}

	return result, nil
}

func IsChangePassword(email string) (string, error) {
	result, err := model.IsChangePassword(email)
	if err != nil {
		return "", err
	}

	return result, nil
}

func ResetPassword(email string, password string) (string, error) {
	result, err := model.ResetPassword(email, password)
	if err != nil {
		return "", err
	}

	return result, nil
}
