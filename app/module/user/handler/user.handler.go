package handler

import (
	"errors"
	"net/http"

	helper "first-restful-api/app/helper"
	userController "first-restful-api/app/module/user/controller"
	validator "first-restful-api/app/module/user/validator"
	migrate "first-restful-api/database"

	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
)

type UserHandler struct {
	DB *gorm.DB
}

func NewUserHandler(DB *gorm.DB) UserHandler {
	return UserHandler{DB: DB}
}

// Register exis
func Create(ctx *gin.Context) {

	userValidator := validator.NewCreateUserValidator()

	if err := userValidator.Bind(ctx); err != nil {
		helper.NewResponse(ctx).ResponseFormatter(http.StatusBadRequest, "Invalid Form", err, gin.H{"error": map[string]string{"binding_error": err.Error()}})

		return
	}

	hashPassword, _ := helper.HashPassword(userValidator.Password)
	_, err := userController.GetUserByName(userValidator.Name)

	if errors.Is(err, gorm.ErrRecordNotFound) {
		newUser := migrate.NewUser()

		newUser.Name = userValidator.Name
		newUser.Email = userValidator.Email
		newUser.Password = hashPassword
		newUser.Phone = userValidator.Phone
		newUser.Address = userValidator.Address

		if err := userController.Save(newUser); err != nil {
			helper.NewResponse(ctx).ResponseFormatter(http.StatusOK, "Successfully Add User", nil, gin.H{
				"data": userValidator,
			})

			return
		}
	}

	helper.NewResponse(ctx).ResponseFormatter(http.StatusBadRequest, "Name Already Existed", nil, gin.H{"error": map[string]string{
		"name_error": "Name Already Existed",
	}})

	return

}
