package model

import (
	config "first-restful-api/app/config"
	helper "first-restful-api/app/helper"
	migrate "first-restful-api/database"
	"fmt"

	_ "github.com/go-sql-driver/mysql"
	"github.com/jinzhu/gorm"
)

//GetAllUsers Fetch all user data
func GetAllUsers(user *[]migrate.User) (err error) {
	if err = config.DB.Find(user).Error; err != nil {
		return err
	}
	return nil
}

//CreateUser ... Insert New data
func CreateUser(user *migrate.User) (err error) {
	if err = config.DB.Create(user).Error; err != nil {
		return err
	}
	return nil
}

func Save(user migrate.User) (result *gorm.DB) {
	result = config.DB.Create(&user)

	return
}

func SaveTmp(user migrate.TmpUser) (result *gorm.DB) {
	result = config.DB.Create(&user)

	return
}

func SaveTmpPassword(data migrate.TmpPassword) (result *gorm.DB) {
	result = config.DB.Create(&data)

	return
}

//GetUserByID ... Fetch only one user by Id
func GetUserByID(user *migrate.User, id string) (err error) {
	if err = config.DB.Where("id = ?", id).First(user).Error; err != nil {
		return err
	}
	return nil
}

// Get Data User By Name
func GetUserByName(name string) (migrate.User, error) {
	getUser := migrate.NewUser()

	result := config.DB.Where("name = ?", name).First(&getUser)
	if result.Error != nil {
		return migrate.User{}, result.Error
	}
	return getUser, nil
}

func GetUserByUsername(username string) (migrate.User, error) {
	getUser := migrate.NewUser()

	result := config.DB.Where("username = ?", username).First(&getUser)
	if result.Error != nil {
		return migrate.User{}, result.Error
	}
	return getUser, nil
}

func GetUserByEmail(email string) (migrate.User, error) {
	getUser := migrate.NewUser()

	result := config.DB.Where("email = ?", email).First(&getUser)
	if result.Error != nil {
		return migrate.User{}, result.Error
	}
	return getUser, nil
}

func GetTmpUserByEmail(email string) (migrate.TmpUser, error) {
	getUser := migrate.NewTmpUser()

	result := config.DB.Where("email = ?", email).First(&getUser)
	if result.Error != nil {
		return migrate.TmpUser{}, result.Error
	}
	return getUser, nil
}

func GetUserByPhone(phone string) (migrate.User, error) {
	getUser := migrate.NewUser()

	result := config.DB.Where("phone = ?", phone).First(&getUser)
	if result.Error != nil {
		return migrate.User{}, result.Error
	}
	return getUser, nil
}

func GetTmpUserByPhone(phone string) (migrate.TmpUser, error) {
	getUser := migrate.NewTmpUser()

	result := config.DB.Where("phone = ?", phone).First(&getUser)
	if result.Error != nil {
		return migrate.TmpUser{}, result.Error
	}
	return getUser, nil
}

func GetUserByFlagSuperadmin(params int) (migrate.User, error) {
	getUser := migrate.NewUser()

	result := config.DB.Where("is_superadmin = ?", params).First(&getUser)
	if result.Error != nil {
		return migrate.User{}, result.Error
	}
	return getUser, nil
}

func GetUserByParams(params string) (migrate.User, error) {
	getUser := migrate.NewUser()

	// result := config.DB.Where("is_verification = ?", 1).Where("email = ? OR phone = ?", params, params).First(&getUser)
	result := config.DB.Where("username = ? OR email = ? OR phone = ?", params, params, params).First(&getUser)
	if result.Error != nil {
		return migrate.User{}, result.Error
	}
	return getUser, nil
}

func GetUserByOtp(userUuid string, otp string) (migrate.User, error) {
	getUser := migrate.NewUser()

	result := config.DB.Select("user.*, otp_trans.code").Joins("left join otp_trans on otp_trans.user_uuid = user.uuid").Where("user.uuid = ? AND otp_trans.code = ?", userUuid, otp).First(&getUser)
	if result.Error != nil {
		return migrate.User{}, result.Error
	}
	return getUser, nil
}

//UpdateUser ... Update user
func UpdateUser(user *migrate.User, id string) (err error) {
	fmt.Println(user)
	config.DB.Save(user)
	return nil
}

//DeleteUser ... Delete user
func DeleteUser(user *migrate.User, id string) (err error) {
	config.DB.Where("id = ?", id).Delete(user)
	return nil
}

func VerifyRegister(email string) (string, error) {

	result := config.DB.Model(&migrate.TmpUser{}).Where("email = ? AND is_verification = ?", email, 0).Update("is_verification", 1)
	if result.Error != nil {
		return "", result.Error
	}
	return email, nil
}

func VerifyResetPassword(email string) (string, error) {

	result := config.DB.Model(&migrate.TmpPassword{}).Where("email = ? AND is_verification = ?", email, 0).Update("is_verification", 1)
	if result.Error != nil {
		return "", result.Error
	}
	return email, nil
}

func GetTmpUserVerifByEmail(email string) (string, error) {
	getUser := migrate.NewTmpUser()

	// result := config.DB.Where("is_verification = ?", 1).Where("email = ? OR phone = ?", params, params).First(&getUser)
	result := config.DB.Where("email = ? AND is_verification = ?", email, 1).First(&getUser)
	if result.Error != nil {
		return "", result.Error
	}
	return email, nil
}

func GetDataTmpUserVerifByEmail(email string) (migrate.TmpUser, error) {
	data := migrate.NewTmpUser()

	// result := config.DB.Where("is_verification = ?", 1).Where("email = ? OR phone = ?", params, params).First(&getUser)
	result := config.DB.Where("email = ? AND is_verification = ?", email, 1).First(&data)
	if result.Error != nil {
		return migrate.TmpUser{}, result.Error
	}
	return data, nil
}

func GetDataTmpPasswordVerifByEmail(email string) (migrate.TmpPassword, error) {
	data := migrate.NewTmpPassword()

	// result := config.DB.Where("is_verification = ?", 1).Where("email = ? OR phone = ?", params, params).First(&getUser)
	result := config.DB.Where("email = ? AND is_verification = ?", email, 1).Order("created_at desc").Limit(1).First(&data)
	if result.Error != nil {
		return migrate.TmpPassword{}, result.Error
	}
	return data, nil
}

func GetDataTmpPasswordChangeByEmail(email string) (migrate.TmpPassword, error) {
	data := migrate.NewTmpPassword()

	// result := config.DB.Where("is_verification = ?", 1).Where("email = ? OR phone = ?", params, params).First(&getUser)
	result := config.DB.Where("email = ? AND is_change = ?", email, 0).Order("created_at desc").Limit(1).First(&data)
	if result.Error != nil {
		return migrate.TmpPassword{}, result.Error
	}
	return data, nil
}

func IsChangePassword(email string) (string, error) {

	result := config.DB.Model(&migrate.TmpPassword{}).Where("email = ? AND is_change = ?", email, 0).Update("is_change", 1)
	if result.Error != nil {
		return "", result.Error
	}
	return email, nil
}

func ResetPassword(email string, password string) (string, error) {

	hashPassword, _ := helper.HashPassword(password)

	result := config.DB.Model(&migrate.User{}).Where("email = ?", email).Update("password", hashPassword)
	if result.Error != nil {
		return "", result.Error
	}
	return password, nil
}
