package router

import (
	userController "first-restful-api/app/module/user/controller"
	userHandler "first-restful-api/app/module/user/handler"

	"github.com/gin-gonic/gin"
)

func Routes(route *gin.Engine) {
	user := route.Group("/api/v1")
	{
		user.GET("user", userController.GetUsers)
		user.POST("user", userHandler.Create)
		user.GET("user/:id", userController.GetUserByID)
		user.PUT("user/:id", userController.UpdateUser)
		user.DELETE("user/:id", userController.DeleteUser)
	}
}
