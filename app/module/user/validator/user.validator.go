package validator

import "github.com/gin-gonic/gin"

type CreateUserValidator struct {
	Name     string `json:"name" binding:"required"`
	Email    string `json:"email" binding:"required"`
	Password string `json:"password" binding:"required"`
	Phone    string `json:"phone" binding:"required"`
	Address  string `json:"address" binding:"required"`
}

func NewCreateUserValidator() CreateUserValidator {
	return CreateUserValidator{}
}

func (userValidator *CreateUserValidator) Bind(c *gin.Context) (err error) {
	err = c.ShouldBindJSON(&userValidator)
	if err != nil {
		return err
	}

	return
}
