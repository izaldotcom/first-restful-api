package database

import (
	"database/sql"
	helper "first-restful-api/app/helper"
	"time"

	"github.com/jinzhu/gorm"
	"gorm.io/plugin/soft_delete"
)

type User struct {
	UUID         string                `gorm:"type:varchar(36);primaryKey;not null" json:"uuid"`
	Name         string                `gorm:"type:varchar(255);null" json:"name"`
	Username     string                `gorm:"type:varchar(255);not null" json:"username"`
	Email        string                `gorm:"type:varchar(255);not null" json:"email"`
	Password     string                `gorm:"type:varchar(255);not null" json:"password"`
	Phone        string                `gorm:"type:varchar(16);not null" json:"phone"`
	Address      string                `gorm:"type:longtext;null" json:"address"`
	IsSuperadmin int8                  `gorm:"default:0;not null" json:"is_superadmin"`
	CreatedAt    time.Time             `gorm:"default:CURRENT_TIMESTAMP" json:"created_at"`
	CreatedBy    sql.NullString        `gorm:"type:varchar(36);null;default:NULL" json:"created_by"`
	UpdatedAt    time.Time             `gorm:"default:CURRENT_TIMESTAMP" json:"updated_at"`
	UpdatedBy    sql.NullString        `gorm:"type:varchar(36);null;default:NULL" json:"updated_by"`
	DeletedAt    soft_delete.DeletedAt `gorm:"type:date;default:NULL" json:"deleted_at"`
	DeletedBy    sql.NullString        `gorm:"type:varchar(36);null;default:NULL" json:"deleted_by"`
}

type TmpUser struct {
	UUID           string                `gorm:"type:varchar(36);primaryKey;not null" json:"uuid"`
	Name           string                `gorm:"type:varchar(255);null" json:"name"`
	Username       string                `gorm:"type:varchar(255);not null" json:"username"`
	Email          string                `gorm:"type:varchar(255);not null" json:"email"`
	Password       string                `gorm:"type:varchar(255);not null" json:"password"`
	Address        string                `gorm:"type:longtext;null" json:"address"`
	IsVerification int8                  `gorm:"default:0;not null" json:"is_verification"`
	CreatedAt      time.Time             `gorm:"default:CURRENT_TIMESTAMP" json:"created_at"`
	CreatedBy      sql.NullString        `gorm:"type:varchar(36);null;default:NULL" json:"created_by"`
	UpdatedAt      time.Time             `gorm:"default:CURRENT_TIMESTAMP" json:"updated_at"`
	UpdatedBy      sql.NullString        `gorm:"type:varchar(36);null;default:NULL" json:"updated_by"`
	DeletedAt      soft_delete.DeletedAt `gorm:"type:date;default:NULL" json:"deleted_at"`
	DeletedBy      sql.NullString        `gorm:"type:varchar(36);null;default:NULL" json:"deleted_by"`
}

type TmpPassword struct {
	UUID           string                `gorm:"type:varchar(36);primaryKey;not null" json:"uuid"`
	Email          string                `gorm:"type:varchar(255);not null" json:"email"`
	IsVerification int8                  `gorm:"default:0;not null" json:"is_verification"`
	IsChange       int8                  `gorm:"default:0;not null" json:"is_change"`
	CreatedAt      time.Time             `gorm:"default:CURRENT_TIMESTAMP" json:"created_at"`
	CreatedBy      sql.NullString        `gorm:"type:varchar(36);null;default:NULL" json:"created_by"`
	UpdatedAt      time.Time             `gorm:"default:CURRENT_TIMESTAMP" json:"updated_at"`
	UpdatedBy      sql.NullString        `gorm:"type:varchar(36);null;default:NULL" json:"updated_by"`
	DeletedAt      soft_delete.DeletedAt `gorm:"type:date;default:NULL" json:"deleted_at"`
	DeletedBy      sql.NullString        `gorm:"type:varchar(36);null;default:NULL" json:"deleted_by"`
}

type UserHasRoles struct {
	UUID     string `gorm:"type:varchar(36);primaryKey;not null" json:"uuid"`
	UserUUID string `gorm:"type:varchar(36);not null;" json:"user_uuid"`
	RoleUUID string `gorm:"type:varchar(36);not null;" json:"role_uuid"`
}

type Role struct {
	UUID      string                `gorm:"type:varchar(36);primaryKey;not null" json:"uuid"`
	Name      string                `gorm:"type:varchar(255);not null" json:"name"`
	CreatedAt time.Time             `gorm:"default:CURRENT_TIMESTAMP" json:"created_at"`
	CreatedBy sql.NullString        `gorm:"type:varchar(36);null;default:NULL" json:"created_by"`
	UpdatedAt time.Time             `gorm:"default:CURRENT_TIMESTAMP" json:"updated_at"`
	UpdatedBy sql.NullString        `gorm:"type:varchar(36);null;default:NULL" json:"updated_by"`
	DeletedAt soft_delete.DeletedAt `gorm:"type:date;default:NULL" json:"deleted_at"`
	DeletedBy sql.NullString        `gorm:"type:varchar(36);null;default:NULL" json:"deleted_by"`
}

type OtpTrans struct {
	UUID        string                `gorm:"type:varchar(36);primaryKey;not null" json:"uuid"`
	Email       string                `gorm:"type:varchar(255);not null;" json:"email"`
	Type        int8                  `gorm:"default:0;not null" json:"type"`
	Code        string                `gorm:"type:varchar(4);not null" json:"code"`
	Phone       string                `gorm:"type:varchar(16);not null" json:"phone"`
	Description string                `gorm:"type:longtext;not null" json:"description"`
	CreatedAt   time.Time             `gorm:"default:CURRENT_TIMESTAMP" json:"created_at"`
	CreatedBy   sql.NullString        `gorm:"type:varchar(36);null;default:NULL" json:"created_by"`
	UpdatedAt   time.Time             `gorm:"default:CURRENT_TIMESTAMP" json:"updated_at"`
	UpdatedBy   sql.NullString        `gorm:"type:varchar(36);null;default:NULL" json:"updated_by"`
	DeletedAt   soft_delete.DeletedAt `gorm:"type:date;default:NULL" json:"deleted_at"`
	DeletedBy   sql.NullString        `gorm:"type:varchar(36);null;default:NULL" json:"deleted_by"`
}

type OtpTransWithUser struct {
	UUID        string `gorm:"type:varchar(36);primaryKey;not null" json:"uuid"`
	UserUUID    string `gorm:"type:varchar(36);not null;" json:"user_uuid"`
	Type        int8   `gorm:"default:0;not null" json:"type"`
	Code        string `gorm:"type:varchar(4);not null" json:"code"`
	Description string `gorm:"type:longtext;not null" json:"description"`
	Name        string `gorm:"type:varchar(255);null" json:"name"`
	Username    string `gorm:"type:varchar(255);not null" json:"username"`
	Email       string `gorm:"type:varchar(255);not null" json:"email"`
	Phone       string `gorm:"type:varchar(255);not null" json:"phone"`
}

type RefreshToken struct {
	UUID      string                `gorm:"type:varchar(36);primaryKey;not null" json:"uuid"`
	UserUUID  string                `gorm:"type:varchar(36);not null;" json:"user_uuid"`
	User      User                  `gorm:"foreignKey:UserUUID;constraint:OnUpdate:CASCADE,OnDelete:CASCADE"`
	Token     string                `gorm:"type:varchar(255);not null;uniqueIndex" json:"token"`
	PhoneID   string                `gorm:"type:varchar(100);not null;uniqueIndex" json:"phone_id"`
	CreatedAt time.Time             `gorm:"default:CURRENT_TIMESTAMP" json:"created_at"`
	CreatedBy sql.NullString        `gorm:"type:varchar(36);null;default:NULL" json:"created_by"`
	UpdatedAt time.Time             `gorm:"default:CURRENT_TIMESTAMP" json:"updated_at"`
	UpdatedBy sql.NullString        `gorm:"type:varchar(36);null;default:NULL" json:"updated_by"`
	DeletedAt soft_delete.DeletedAt `gorm:"type:date;default:NULL" json:"deleted_at"`
	DeletedBy sql.NullString        `gorm:"type:varchar(36);null;default:NULL" json:"deleted_by"`
}

type Product struct {
	UUID      string                `gorm:"type:varchar(36);primaryKey;not null" json:"uuid"`
	Name      string                `gorm:"type:varchar(255);not null" json:"name"`
	Price     float64               `gorm:"column:price;not null" json:"price"`
	Qty       int64                 `gorm:"column:qty;not null" json:"qty"`
	CreatedAt time.Time             `gorm:"default:CURRENT_TIMESTAMP" json:"created_at"`
	CreatedBy sql.NullString        `gorm:"type:varchar(36);null;default:NULL" json:"created_by"`
	UpdatedAt time.Time             `gorm:"default:CURRENT_TIMESTAMP" json:"updated_at"`
	UpdatedBy sql.NullString        `gorm:"type:varchar(36);null;default:NULL" json:"updated_by"`
	DeletedAt soft_delete.DeletedAt `gorm:"type:date;default:NULL" json:"deleted_at"`
	DeletedBy sql.NullString        `gorm:"type:varchar(36);null;default:NULL" json:"deleted_by"`
}

func (b *User) TableName() string {
	return "user"
}

func (b *TmpUser) TableName() string {
	return "tmp_user"
}

func (b *TmpPassword) TableName() string {
	return "tmp_password"
}

func (b *Role) TableName() string {
	return "role"
}

func (b *UserHasRoles) TableName() string {
	return "user_has_roles"
}

func (b *OtpTrans) TableName() string {
	return "otp_trans"
}

func (b *RefreshToken) TableName() string {
	return "refresh_token"
}

func (b *Product) TableName() string {
	return "product"
}

func (b *User) BeforeCreate(tx *gorm.DB) (err error) {
	b.UUID = helper.GenerateUuid()

	if !helper.ValidateUuid(b.UUID) {
		return err
	}

	return nil
}

func (b *TmpUser) BeforeCreate(tx *gorm.DB) (err error) {
	b.UUID = helper.GenerateUuid()

	if !helper.ValidateUuid(b.UUID) {
		return err
	}

	return nil
}

func (b *TmpPassword) BeforeCreate(tx *gorm.DB) (err error) {
	b.UUID = helper.GenerateUuid()

	if !helper.ValidateUuid(b.UUID) {
		return err
	}

	return nil
}

func (b *Role) BeforeCreate(tx *gorm.DB) (err error) {
	b.UUID = helper.GenerateUuid()

	if !helper.ValidateUuid(b.UUID) {
		return err
	}

	return nil
}

func (b *UserHasRoles) BeforeCreate(tx *gorm.DB) (err error) {
	b.UUID = helper.GenerateUuid()

	if !helper.ValidateUuid(b.UUID) {
		return err
	}

	return nil
}

func (b *OtpTrans) BeforeCreate(tx *gorm.DB) (err error) {
	b.UUID = helper.GenerateUuid()

	if !helper.ValidateUuid(b.UUID) {
		return err
	}

	return nil
}

func (b *RefreshToken) BeforeCreate(tx *gorm.DB) (err error) {
	b.UUID = helper.GenerateUuid()
	b.Token = helper.GenerateSecureToken(36)

	if !helper.ValidateUuid(b.UUID) {
		return err
	}

	return nil
}

func (b *Product) BeforeCreate(tx *gorm.DB) (err error) {
	b.UUID = helper.GenerateUuid()

	if !helper.ValidateUuid(b.UUID) {
		return err
	}

	return nil
}

func NewUser() User {
	return User{}
}

func NewTmpUser() TmpUser {
	return TmpUser{}
}

func NewTmpPassword() TmpPassword {
	return TmpPassword{}
}

func NewRole() Role {
	return Role{}
}

func NewUserHasRoles() UserHasRoles {
	return UserHasRoles{}
}

func NewOtpTrans() OtpTrans {
	return OtpTrans{}
}

// func NewOtpTransWithUser() OtpTransWithUser {
// 	return OtpTransWithUser{}
// }

func NewRefreshToken() RefreshToken {
	return RefreshToken{}
}

func NewProduct() Product {
	return Product{}
}
