package seeder

import (
	helper "first-restful-api/app/helper"
	roleController "first-restful-api/app/module/role/controller"
	userController "first-restful-api/app/module/user/controller"

	"github.com/jinzhu/gorm"
)

type Seed struct {
	Name string
	Run  func(*gorm.DB) error
}

// SeedsAll will call all your filled func seeds
func SeedAllRole() []Seed {
	return []Seed{
		// Seeder Role
		{
			Name: "SeederRole",
			Run: func(db *gorm.DB) error {
				SeederRole(db, "customer")
				return nil
			},
		},
		{
			Name: "SeederRole",
			Run: func(db *gorm.DB) error {
				SeederRole(db, "business_account")
				return nil
			},
		},
		{
			Name: "SeederRole",
			Run: func(db *gorm.DB) error {
				SeederRole(db, "admin")
				return nil
			},
		},
		{
			Name: "SeederRole",
			Run: func(db *gorm.DB) error {
				SeederRole(db, "superadmin")
				return nil
			},
		},
	}
}

func SeedAllUser() []Seed {
	// hassPassword
	password, _ := helper.HashPassword("12345678")

	return []Seed{
		// Seeder User
		{
			Name: "SeederUser",
			Run: func(db *gorm.DB) error {
				SeederUser(db, "Rifky Fahrizal", "izaldotcom", "rifkyfahrizal22@gmail.com", password, "+6282111662745", "Jakarta", 1)
				return nil
			},
		},
	}
}

func SeedUserHasRole() []Seed {
	// get user is superadmin
	getUser, _ := userController.GetUserByFlagSuperadmin(1)
	userUuid := getUser.UUID

	// get role with name is superadmin
	getRole := roleController.GetRoleByName("superadmin")
	roleUuid := getRole.UUID

	return []Seed{
		// Seeder User
		{
			Name: "SeederUserHasRoles",
			Run: func(db *gorm.DB) error {
				SeederUserHasRoles(db, userUuid, roleUuid)
				return nil
			},
		},
	}
}
