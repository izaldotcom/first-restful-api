package seeder

import (
	"database/sql"
	"errors"
	migrate "first-restful-api/database"

	"github.com/jinzhu/gorm"
)

func SeederRole(db *gorm.DB, name string) error {
	var (
		checkRole migrate.Role
		err       error
	)

	newRole := migrate.Role{
		Name: name,
	}

	newRole.CreatedBy = sql.NullString{String: "seeders", Valid: true}
	newRole.UpdatedBy = sql.NullString{String: "seeders", Valid: true}

	if err = db.First(&checkRole, "name=?", name).Error; err != nil {

		result := db.Create(&newRole)
		if result.RowsAffected == 0 {
			return errors.New("failed to create role " + newRole.Name)
		}

		return nil
	}

	return errors.New("role" + newRole.Name + " already existed in database")
}
