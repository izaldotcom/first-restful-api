package seeder

import (
	"database/sql"
	"errors"
	migrate "first-restful-api/database"

	"github.com/jinzhu/gorm"
)

func SeederUser(db *gorm.DB, name string, username string, email string, password string, phone string, address string, is_superadmin int8) error {
	var (
		checkRole migrate.User
		err       error
	)

	newUser := migrate.User{
		Name:         name,
		Username:     username,
		Email:        email,
		Password:     password,
		Phone:        phone,
		Address:      address,
		IsSuperadmin: is_superadmin,
	}

	newUser.CreatedBy = sql.NullString{String: "seeders", Valid: true}
	newUser.UpdatedBy = sql.NullString{String: "seeders", Valid: true}

	if err = db.First(&checkRole, "username=?", username).Error; err != nil {

		result := db.Create(&newUser)
		if result.RowsAffected == 0 {
			return errors.New("failed to create user " + newUser.Username)
		}

		return nil
	}

	return errors.New("Username" + newUser.Username + " already existed in database")
}
