package seeder

import (
	"errors"
	migrate "first-restful-api/database"

	"github.com/jinzhu/gorm"
)

func SeederUserHasRoles(db *gorm.DB, user_uuid string, role_uuid string) error {
	var (
		checkData migrate.UserHasRoles
		err       error
	)

	newData := migrate.UserHasRoles{
		UserUUID: user_uuid,
		RoleUUID: role_uuid,
	}

	if err = db.First(&checkData, "user_uuid=?", user_uuid).Error; err != nil {

		result := db.Create(&newData)
		if result.RowsAffected == 0 {
			return errors.New("failed to create data " + newData.UserUUID)
		}

		return nil
	}

	return errors.New("Data" + newData.UserUUID + " already existed in database")
}
