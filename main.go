package main

import (
	config "first-restful-api/app/config"
	authRouter "first-restful-api/app/module/auth/router"
	productRouter "first-restful-api/app/module/product/router"
	userRouter "first-restful-api/app/module/user/router"
	migrate "first-restful-api/database"
	seeder "first-restful-api/database/seeder"
	"fmt"
	"log"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	"github.com/joho/godotenv"
)

var err error

func Seed(db *gorm.DB) {
	for _, seed := range seeder.SeedAllRole() {
		if err := seed.Run(db); err != nil {
			log.Fatalf("Running seed '%s', failed with error: %s", seed.Name, err)
		}
	}
	for _, seed := range seeder.SeedAllUser() {
		if err := seed.Run(db); err != nil {
			log.Fatalf("Running seed '%s', failed with error: %s", seed.Name, err)
		}
	}
	for _, seed := range seeder.SeedUserHasRole() {
		if err := seed.Run(db); err != nil {
			log.Fatalf("Running seed '%s', failed with error: %s", seed.Name, err)
		}
	}
}

func main() {
	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file, please create one in the root directory")
	}

	loc, err := time.LoadLocation("Asia/Jakarta")
	if err != nil {
		log.Println(err)
		return
	}
	time.Local = loc

	config.DB, err = gorm.Open("mysql", config.DbURL(config.BuildDBConfig()))
	if err != nil {
		fmt.Println("Status:", err)
	}
	defer config.DB.Close()

	// running migrate
	config.DB.AutoMigrate(&migrate.User{})
	config.DB.AutoMigrate(&migrate.TmpUser{})
	config.DB.AutoMigrate(&migrate.TmpPassword{})
	config.DB.AutoMigrate(&migrate.UserHasRoles{})
	config.DB.AutoMigrate(&migrate.Role{})
	config.DB.AutoMigrate(&migrate.OtpTrans{})
	config.DB.AutoMigrate(&migrate.RefreshToken{})
	config.DB.AutoMigrate(&migrate.Product{})

	// running seed
	Seed(config.DB)

	// Multiple Router
	router := gin.Default()
	productRouter.Routes(router)
	userRouter.Routes(router)
	authRouter.Routes(router)
	router.Run()
}
